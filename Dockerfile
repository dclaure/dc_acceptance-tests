FROM openjdk:11.0.11-jdk-slim-buster
WORKDIR /app

# Copy Gradle project
COPY settings.gradle .
COPY build.gradle .
COPY src src/
COPY gradle gradle/
# COPY images images/
COPY gradlew .

# Run tests
CMD ["./gradlew","clean","test","--console","verbose"]
